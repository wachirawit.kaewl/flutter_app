import 'dart:math';
import 'cat.dart';
import 'catTypes.dart';

 class CatHouse {
   static List<Cat> cats;
  static List<Cat> removeCats;

  void removeCat() {
    removeCats.forEach((Cat cat) {
      cats.remove(cat);
    });
  }

   static void initCatHouse() {
    CatHouse.cats = List<Cat>();
    CatHouse.removeCats = List<Cat>();
    addCat(CatTypes.SIMPLE);
  }

   static void addCat(CatTypes type) {
    switch(type){
      case CatTypes.SIMPLE:
        cats.add(new Cat(simpleCat, 10, 20));
        break;
    }
  }

  static CatTypes randomCatType() {
    Random r = new Random();
    return  CatTypes.values[r.nextInt(CatTypes.values.length)] ;
  }
}
