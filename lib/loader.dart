import 'package:flame/flame.dart';

import 'catTypes.dart';

void loadImages() async {
  await Flame.images.load(simpleCat);
}