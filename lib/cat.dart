import 'dart:ui';
import 'package:flame/components/component.dart';
import 'package:flame/components/mixins/tapable.dart';
import 'package:flame/sprite.dart';
import 'package:flutter/material.dart';
import 'catTypes.dart';

class Cat extends SpriteComponent with Tapable  {
  Sprite sprite;

  Cat(String name, double dx, double dy) {
    sprite  = new Sprite(name, x:dx, y:dy) ;
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    sprite.render(canvas);
  }

  @override
  void update(double dt) {
    super.update(dt);
    sprite.src = sprite.src.shift(Offset(-1,-1));
  }

  @override
  void onTapDown(TapDownDetails details) {
  }
}

