import 'package:flame/game.dart';
import 'package:flame/gestures.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/catHouse.dart';
import 'package:flutter_app/states.dart';
import 'background.dart';
import 'loader.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final game = MyGame();
  loadImages();
  runApp(game.widget);
}

class MyGame extends BaseGame with TapDetector {
  Size screenSize;
  double horizonTile, verticalTile;
  States states = States.PLAYING;
  final TextConfig config = TextConfig(fontSize: 22.0, color: Colors.white);

  MyGame() {
    CatHouse.initCatHouse();
  }
  void render(Canvas canvas) {
    super.render(canvas);
    drawBG(canvas, size);
    config.render(canvas, "Flame is awesome", Position(horizonTile * 4.5, verticalTile)); //Text

    switch(states) {
      case States.LOADING:
        break;
      case States.PLAYING:
        CatHouse.cats.forEach((cat) { cat.render(canvas);});
        break;
      case States.PAUSE:
        break;
    }
  }

  void update(double t) {
    super.update(t);
    CatHouse.cats.forEach((cat) { cat.update(t);});
  }

  void resize(Size size) {
    super.resize(size);
    screenSize = size;
    horizonTile = size.width / 9;
    verticalTile = screenSize.height / 16;
  }

  void onTapDown(TapDownDetails details) {
    }
}

