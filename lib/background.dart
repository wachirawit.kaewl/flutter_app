import 'dart:ui';
import 'package:flutter/material.dart';

void drawBG(Canvas canvas, Size screenSize) {
  Rect bgRect = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
  Paint bgPaint = Paint();
  bgPaint.color = Colors.white30;
  canvas.drawRect(bgRect, bgPaint);
}